
describe('Canvas', function(){

  it('should throw errors if no canvas element is initiated', function(){

    expect(function(){
      new Canvas('specCanvas')
    }).toThrowError();

  });

});