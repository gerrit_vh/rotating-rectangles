
describe('Storage', function(){

  afterEach(function(){
    storage.destroy('spec');
  });

  it('should save and retrieve a value', function(){

    storage.set('spec', {'foo': 'bar'});

    expect(storage.get('spec')).toEqual({'foo': 'bar'});

  });

});