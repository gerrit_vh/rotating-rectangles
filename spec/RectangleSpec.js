
describe("Rectangle", function() {

  var rectangle;

  beforeEach(function(){
    rectangle = new Rectangle();
    rectangle.drawRandom(0, 0, 500, 250);
  });

  afterEach(function(){
    rectangle = null;
  });

  describe('Width and Height', function(){

    it('should be defined', function(){

      expect(rectangle.height).toBeDefined();
      expect(rectangle.width).toBeDefined();

    });

    it('should always be positive', function(){

      expect(rectangle.height).toBeGreaterThan(0);
      expect(rectangle.width).toBeGreaterThan(0);

    });

    it('should NEVER be equal', function(){

      expect(rectangle.height).not.toEqual(rectangle.width);

    });

  });

  describe('Location', function(){

    it('should be defined', function(){

      expect(rectangle.startX).toBeDefined();
      expect(rectangle.startY).toBeDefined();

      expect(rectangle.endX).toBeDefined();
      expect(rectangle.endY).toBeDefined();

    });

    it('End location should be start location plus length', function(){

      expect(rectangle.startX + rectangle.width).toEqual(rectangle.endX);
      expect(rectangle.startY + rectangle.height).toEqual(rectangle.endY);

    });

    it('negative start values should throw error', function(){

      // startX is negative
      expect(function(){
        rectangle.drawRandom(-1, 0, 0, 0);
      }).toThrowError();

      // startY is negative
      expect(function(){
        rectangle.drawRandom(0, -1, 0, 0);
      }).toThrowError();

    });

  });


});