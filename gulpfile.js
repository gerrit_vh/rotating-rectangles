'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var minify = require('gulp-minify');

/*
 Default
 */

gulp.task('default', ['sass', 'scripts']);
gulp.task('watch', ['sass:watch', 'scripts:watch']);


/*
 SASS Files
 */

gulp.task('sass', function(){

  gulp.src('./sass/**/*.scss')
      .pipe(sass.sync().on('error', sass.logError))
      .pipe(sass({outputStyle: 'compressed'}))
      .pipe(gulp.dest('./public/css'));

});

gulp.task('sass:watch', function () {
  gulp.watch('./sass/**/*.scss', ['sass']);
});


/*
 Javascript Files
 */

gulp.task('scripts', function() {
  gulp.src('./src/*.js')
      .pipe(concat('all.js'))
      .pipe(minify())
      .pipe(gulp.dest('./public/js'));
});

gulp.task('scripts:watch', function () {
  gulp.watch('./src/**/*.js', ['scripts']);
});