# Setup

#### Gulp

If gulp is not installed already, please install it and all of its dependencies:

```sh
$ npm install --global gulp
$ npm install --save-dev gulp
$ npm install --save-dev gulp-sass
$ npm install --save-dev gulp-concat
$ npm install --save-dev gulp-minify
```

To concatenate javascript and compile SASS run the following command:

```sh
$ gulp
```

To watch any file changes in javascript or SASS:

```sh
$ gulp watch
```

# Tests

I'm using Jasmine to implement tests

To run all tests, open `SpecRunner.html` in your browser.