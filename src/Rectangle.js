
var rectangle = new Rectangle();

function Rectangle(){

  this.startX = 0;
  this.startY = 0;

  this.endX = 0;
  this.endY = 0;

  this.width = 500;
  this.height = 250;

}

Rectangle.prototype.drawRandom = function(startX, startY, maxWidth, maxHeight){

  if(startX < 0)
    throw new Error('startX can not be negative');

  if(startY < 0)
    throw new Error('startY can not be negative');

  this.width = this.randomLength(maxWidth);
  this.height = this.randomLength(maxHeight);

  if(this.height === this.width)
    return this.drawRandom(startX, startY, maxWidth, maxHeight);

  this.startX = startX;
  this.startY = startY;

  this.endX = this.startX + this.width;
  this.endY = this.startY + this.height;

};

Rectangle.prototype.randomlyDrawMany = function(count, canvasWidth, canvasHeight){

  this.canvasStartX = 0;
  this.canvasStartY = 0;

  var rectangleCollection = [];

  var maxRectangleWidth = canvasWidth / count;
  var maxRectangleHeight = canvasHeight;

  for(var i = 0; i < count; i++){

    this.drawRandom(this.canvasStartX, this.canvasStartY, maxRectangleWidth, maxRectangleHeight);

    var properties = {};
    properties['width'] = this.width;
    properties['height'] = this.height;
    properties['startX'] = this.startX;
    properties['startY'] = canvasHeight - this.height;
    properties['endX'] = this.endX;
    properties['endY'] = this.endY;

    this.canvasStartX = properties['endX'];
    this.canvasStartY = properties['endY'];

    rectangleCollection.push(properties);

  }

  storage.set('inputRectangles', rectangleCollection);

  return rectangleCollection;

};

Rectangle.prototype.calculateOutputRectangles = function(){

  var sortedByHeight = storage.get('inputRectangles').sort(function(a, b){ return a.height - b.height; });

  var verticalRectangles = [];

  for(var i = 0; i < sortedByHeight.length; i++) {

    var newRectangle = this.createCustomRectangle(
        sortedByHeight[i].startX,
        sortedByHeight[i].startY,
        sortedByHeight[i].width,
        sortedByHeight[i].height
    );

    if(typeof sortedByHeight[i - 1] !== 'undefined'){
      newRectangle.height -= sortedByHeight[i - 1].height;
    }

    var sortedByStartX = storage.get('inputRectangles').sort(function (a, b) {
      return a.startX - b.startX;
    });

    for (var ii = 0; ii < sortedByStartX.length; ii++) {

      if (
          sortedByHeight[i].startX < sortedByStartX[ii].startX &&
          sortedByHeight[i].height < sortedByStartX[ii].height
      ) {


        newRectangle.width += sortedByStartX[ii].width;

        if (newRectangle.startX > sortedByStartX[ii].startX) {
          newRectangle.startX = sortedByStartX[ii].startX;
        }

      }

    }

    verticalRectangles.push(newRectangle);

  }

  var finalRectangles = [];

  // TODO: Unsure of this step. Work back to fill the gaps
  for(var iii = 0; iii < verticalRectangles.length; iii++){

    if(typeof verticalRectangles[iii + 1] !== 'undefined'){

      if(verticalRectangles[iii].startY < (verticalRectangles[iii + 1].startY + verticalRectangles[iii + 1].height)){

        verticalRectangles[iii].width += verticalRectangles[iii + 1].width;

        verticalRectangles[iii].startX = verticalRectangles[iii + 1].startX;

      }

    }

    finalRectangles.push(verticalRectangles[iii]);

  }

  storage.set('outputRectangles', finalRectangles);

};

Rectangle.prototype.calculateFoundation = function(rectangles){

  var shortestRectangleIndex = 0;
  var shortestRectangle = 0;
  var lastRectangleEndX = 0;

  for(var i = 0; i < rectangles.length; i++){

    // Set first rectangle as the shortest rectangle
    if(i == 0){
      shortestRectangle = rectangles[0].height;
      continue;
    }

    // Find the shortest rectangle
    if(rectangles[i].height < shortestRectangle){
      shortestRectangleIndex = i;
      shortestRectangle = rectangles[i].height;
    }

    lastRectangleEndX = rectangles[i].endX;

  }

  rectangles[shortestRectangleIndex].startX = 0;
  rectangles[shortestRectangleIndex].width = lastRectangleEndX;

  return rectangles[shortestRectangleIndex];

};

Rectangle.prototype.createCustomRectangle = function(startX, startY, width, height){

  var newRectangle = {};
  newRectangle.startX = startX;
  newRectangle.startY = startY;
  newRectangle.width = width;
  newRectangle.height = height;

  return newRectangle;

};

Rectangle.prototype.randomLength = function(max){

  var min = 5;

  return Math.floor(Math.random() * (max - min)) + min;

};