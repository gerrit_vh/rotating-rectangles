
function Canvas(id){

  if(typeof id === 'undefined')
    throw new Error('Canvas ID is not set');

  this.id = id;

  this.element = document.getElementById(this.id);

  if(this.element == null)
    throw new Error('Could not find element with id: ' + this.id);

  this.height = this.element.offsetHeight;
  this.width = this.element.offsetWidth;

  this.context = this.element.getContext("2d");

}

Canvas.prototype.drawFromLocalStorage = function(localStorageKey){

  var rectangles = storage.get(localStorageKey);

  if(rectangles == null)
    throw new Error('Local Storage key "' + localStorageKey + '" is not set');

  if(typeof this.element === 'undefined')
    throw new Error('Canvas have not been created');

  if(typeof this.context === 'undefined')
    throw new Error('Canvas context was not retrieved');

  if(typeof rectangles !== 'object')
    throw new Error('Parameter should be an object of rectangles');

  // Clear canvas before drawing new set of rectangles
  this.context.clearRect(0, 0, this.width, this.height);

  for(var i = 0; i < rectangles.length; i++){

    this.context.strokeRect(rectangles[i].startX, rectangles[i].startY, rectangles[i].width, rectangles[i].height);

  }

};