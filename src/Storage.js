
var storage = new Storage();

function Storage(){}

Storage.prototype.set = function(key, value){
  localStorage.setItem(key, JSON.stringify(value));
};

Storage.prototype.get = function(key){
  return JSON.parse(localStorage.getItem(key));
};

Storage.prototype.destroy = function(key){
  localStorage.clear(key);
};